@extends('main_menu.main')

@section('container')
<div class="container-fluid px-4">
   <h1 class="mt-4">Create New Product Category</h1>
   <div class="card col-lg-8" style="padding: 13px; max-width: 30rem; box-shadow: 6px 8px rgb(0 0 0 / 4%); border: 1">
      <form action="{{ route('product-category.store') }}" method="POST" class="mb-3" enctype="multipart/form-data">
      @csrf
      <div class="mb-3">
         <label for="name_category" class="form-label">Name Category</label>
         <input type="text" class="form-control @error('name_category') is-invalid @enderror" id="name_category" name="name_category" placeholder="name category" required>
         @error('name_category')
            <div class="invalid-feedback">
               {{ $message }}
            </div>
         @enderror
      </div>
         <button type="submit" class="btn btn-primary">Create New Product Category</button>
      </form>
   </div>   
</div>
@endsection