@extends('main_menu.main')

@section('container')
<div class="container-fluid px-4">
   <h1 class="mt-4">Edit Product Category</h1>
   <div class="card col-lg-8" style="padding: 13px; max-width: 30rem; box-shadow: 6px 8px rgb(0 0 0 / 4%); border: 1">
      <form action="{{ route('product-category.update', $productCategory->id) }}" method="POST" class="mb-3" enctype="multipart/form-data">
      @csrf
      @method('PUT')
      <div class="mb-3">
         <label for="name_category" class="form-label">Name Category</label>
         <input type="text" class="form-control @error('name_category') is-invalid @enderror" id="name_category" name="name_category" placeholder="name category" required value="{{ $productCategory->name_category }}">
         @error('name_category')
            <div class="invalid-feedback">
               {{ $message }}
            </div>
         @enderror
      </div>
         <button type="submit" class="btn btn-primary">Edit Product Category</button>
      </form>
   </div>   
</div>
@endsection