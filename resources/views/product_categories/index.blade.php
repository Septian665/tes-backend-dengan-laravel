@extends('main_menu.main')

@section('container')
<div class="table-responsive col-lg-6">
  <a href="{{ route('product-category.create') }}" class="btn btn-primary mb-3">Create New Variant</a>
  @if (session()->has('success'))
    <div class="alert alert-success col-lg-6 alert-dismissible fade show" role="alert">
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      {{ session('success') }}
    </div>
  @endif
    <table class="table table-bordered">
      <thead>
         <tr>
            <th scope="col">No</th>
            <th scope="col">Name Category</th>
            <th scope="col">Action</th>
         </tr>
      </thead>
      <tbody>
         @foreach ($ProductCategory as $productcategory)
         <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $productcategory->name_category }}</td>
            <td>
               <a href="{{ route('product-category.edit', $productcategory->id) }}" class="badge bg-warning">
                  <span data-feather="edit">Edit</span>
               </a>
               <form action="{{ route('product-category.destroy', $productcategory->id) }}" method="POST" class="d-inline">
                  @method('delete')
                  @csrf
                  <button class="badge bg-danger border-0"><span data-feather="x-circle">Delete</span></button>
               </form>
            </td>
         </tr>
        @endforeach
      </tbody>
    </table>
</div>
@endsection