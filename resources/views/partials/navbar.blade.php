<nav class="navbar navbar-expand-lg bg-danger navbar-dark">
   <div class="container">
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
         <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
         <ul class="navbar-nav">
            <li class="nav-item">
               <a class="nav-link {{ Request::is('variant') ? 'active' : '' }}" href="{{ route('variant.index') }}">Variants</a>
            </li>
            <li class="nav-item">
               <a class="nav-link {{ Request::is('product-category') ? 'active' : '' }}" href="{{ route('product-category.index') }}">Product Categories</a>
            </li>
            <li class="nav-item">
               <a class="nav-link {{ Request::is('product-image') ? 'active' : '' }}" href="{{ route('product-image.index') }}">Product Image</a>
            </li>
            <li class="nav-item">
               <a class="nav-link {{ Request::is('product') ? 'active' : '' }}" href="{{ route('product.index') }}">Product</a>
            </li>
         </ul>
      </div>
   </div>
</nav>