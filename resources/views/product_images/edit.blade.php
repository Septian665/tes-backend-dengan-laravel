@extends('main_menu.main')

@section('container')
<div class="container-fluid px-4">
   <h1 class="mt-4">Edit Product Image</h1>
   <div class="card col-lg-8" style="padding: 13px; max-width: 30rem; box-shadow: 6px 8px rgb(0 0 0 / 4%); border: 1">
      <form action="{{ route('product-image.update', $productimage->id) }}" method="POST" class="mb-3" enctype="multipart/form-data">
      @csrf
      @method('PUT')
      <div class="mb-3">
         <label for="name_image" class="form-label">Name Image</label>
         <input type="text" class="form-control @error('name_image') is-invalid @enderror" id="name_image" name="name_image" placeholder="name image" required value="{{ $productimage->name_image }}">
         @error('name_image')
            <div class="invalid-feedback">
               {{ $message }}
            </div>
         @enderror
      </div>
      <div class="mb-3">
         <label for="image" class="form-label">Upload Image</label>
         <input class="form-control @error('image') is-invalid @enderror" type="file" id="image" name="image">
         @error('image')
            <div class="invalid-feedback">
               {{ $message }}
            </div>
         @enderror
      </div>
         <button type="submit" class="btn btn-primary">Edit Product Image</button>
      </form>
   </div>   
</div>
@endsection