@extends('main_menu.main')

@section('container')
{{-- <p>{{ $product->product_category->id }}</p> --}}
<div class="table-responsive">
   <a href="{{ route('product.create') }}" class="btn btn-primary mb-3">Create New Product</a>
   @if (session()->has('success'))
      <div class="alert alert-success col-lg-6 alert-dismissible fade show" role="alert">
         <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
         {{ session('success') }}
      </div>
   @endif
   <div class="table-resposive">
      <table class="table table-bordered">
         <thead>
            <tr>
               <th>No</th>
               <th>Name Product</th>
               <th>Description</th>
               <th>Price</th>
               <th>Name Variant</th>
               <th>Product Category</th>
               <th>Product Image</th>
               <th>Action</th>
            </tr>
         </thead>
         <tbody>
            @foreach ($product as $dataproduct)
            <tr>
               <td style="width: 5%">{{ $loop->iteration }}</td>
               <td style="width: 12%">{{ $dataproduct->name_product }}</td>
               <td style="width: 15%">{{ $dataproduct->description }}</td>
               <td style="width: 8%">{{ $dataproduct->price }}</td>
               <td style="width: 10%">
                  {{ $dataproduct->variant->size }},
                  {{ $dataproduct->variant->color }}
               </td>
               <td>{{ $dataproduct->ProductCategory->name_category }}</td>
               <td class="text-center">
                  <img src="{{ asset('storage/'.$dataproduct->ProductImage->image) }}" class="rounded" style="width: 100px">
               </td>
               <td style="width: 12%">
                  <a href="{{ route('product.edit', $dataproduct->id) }}" class="badge bg-warning">
                     <span data-feather="edit">Edit</span>
                  </a>
                  <form action="{{ route('product.destroy', $dataproduct->id) }}" method="POST" class="d-inline">
                     @method('delete')
                     @csrf
                     <button class="badge bg-danger border-0"><span data-feather="x-circle">Delete</span></button>
                  </form>
               </td>
            </tr>
           @endforeach
         </tbody>
      </table>
   </div>
</div>
@endsection