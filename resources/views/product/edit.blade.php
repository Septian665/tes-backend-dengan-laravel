@extends('main_menu.main')

@section('container')
<div class="container-fluid px-4">
   <h1 class="mt-4">Edit Product</h1>
   <div class="card col-lg-8" style="padding: 13px; max-width: 30rem; box-shadow: 6px 8px rgb(0 0 0 / 4%); border: 1">
      <form action="{{ route('product.update', $product->id) }}" method="POST" class="mb-3" enctype="multipart/form-data">
      @csrf
      @method('PUT')
      <div class="mb-3">
         <label for="name_product" class="form-label">Name Product</label>
         <input type="text" class="form-control @error('name_product') is-invalid @enderror" id="name_product" name="name_product" placeholder="name category" required value="{{ $product->name_product }}">
         @error('name_product')
            <div class="invalid-feedback">
               {{ $message }}
            </div>
         @enderror
      </div>
      <div class="mb-3">
         <label for="description" class="form-label">Description</label>
         <input type="text" class="form-control @error('description') is-invalid @enderror" id="description" name="description" placeholder="description" required value="{{ $product->description }}">
         @error('description')
            <div class="invalid-feedback">
               {{ $message }}
            </div>
         @enderror
      </div>
      <div class="mb-3">
         <label for="price" class="form-label">Price</label>
         <input type="text" class="form-control @error('price') is-invalid @enderror" id="price" name="price" placeholder="price" required value="{{ $product->price }}">
         @error('price')
            <div class="invalid-feedback">
               {{ $message }}
            </div>
         @enderror
      </div>
      <div class="mb-3">
         <label for="name_variantId" class="form-label">Name Variant</label>
         <select class="form-select @error('name_variantId') is-invalid @enderror" aria-label="Default select example" name="name_variantId" id="name_variantId" required>
            <option value="{{ $product->variant->id }},{{ $product->variant->color }}">{{ $product->variant->size }},{{ $product->variant->color }}</option>
            @foreach ($variants as $datavariant)
               <option value="{{ $datavariant->id }}">{{ $datavariant->size }},{{ $datavariant->color }}</option>
            @endforeach
         </select>
         @error('name_variantId')
           <div class="invalid-feedback">
             {{ $message }}
           </div>
         @enderror
      </div>
      <div class="mb-3">
         <label for="product_categoryId" class="form-label">Name Category</label>
         <select class="form-select @error('product_categoryId') is-invalid @enderror" aria-label="Default select example" name="product_categoryId" id="product_categoryId" required>
           <option selected value="{{ $product->ProductCategory->id }}">{{ $product->ProductCategory->name_category }}</option>
           @foreach ($productategoryList as $dataproductcategory)
               <option value="{{ $dataproductcategory->id }}">{{ $dataproductcategory->name_category }}</option>
           @endforeach
         </select>
         @error('product_categoryId')
           <div class="invalid-feedback">
             {{ $message }}
           </div>
         @enderror
      </div>
      <div class="mb-3">
         <label for="product_imageId" class="form-label">Name Image</label>
         <select class="form-select @error('product_imageId') is-invalid @enderror" aria-label="Default select example" name="product_imageId" id="product_imageId" required>
            <option selected value="{{ $product->ProductImage->id }}">{{ $product->ProductImage->name_image }}</option>
            @foreach ($productimageList as $dataproductimage)
               <option value="{{ $dataproductimage->id }}">{{ $dataproductimage->name_image }}</option>  
            @endforeach
         </select>
         @error('product_imageId')
            <div class="invalid-feedback">
               {{ $message }}
            </div>
         @enderror
      </div>
         <button type="submit" class="btn btn-primary">Edit Product</button>
      </form>
   </div>   
</div>
@endsection