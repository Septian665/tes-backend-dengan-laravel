@extends('main_menu.main')

@section('container')
<div class="container-fluid px-4">
   <h1 class="mt-4">Create New Variant</h1>
   <div class="card col-lg-8" style="padding: 13px; max-width: 30rem; box-shadow: 6px 8px rgb(0 0 0 / 4%); border: 1">
      <form action="{{ route('variant.store') }}" method="POST" class="mb-3" enctype="multipart/form-data">
      @csrf
      <div class="mb-3">
         <label for="size" class="form-label">Size</label>
         <input type="text" class="form-control @error('size') is-invalid @enderror" id="size" name="size" placeholder="Size" required>
         @error('size')
            <div class="invalid-feedback">
               {{ $message }}
            </div>
         @enderror
      </div>
      <div class="mb-3">
         <label for="color" class="form-label">Color</label>
         <input type="text" class="form-control @error('color') is-invalid @enderror" id="color" name="color" placeholder="Color" required>
         @error('color')
            <div class="invalid-feedback">
               {{ $message }}
            </div>
         @enderror
      </div>
         <button type="submit" class="btn btn-primary">Create New Variant</button>
      </form>
   </div>   
</div>
@endsection