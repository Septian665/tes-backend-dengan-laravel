@extends('main_menu.main')

@section('container')
<div class="table-responsive col-lg-6">
  <a href="{{ route('variant.create') }}" class="btn btn-primary mb-3">Create New Variant</a>
  @if (session()->has('success'))
    <div class="alert alert-success col-lg-6 alert-dismissible fade show" role="alert">
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      {{ session('success') }}
    </div>
  @endif
    <table class="table table-bordered">
      <thead>
        <tr>
          <th scope="col">No</th>
          <th scope="col">Size</th>
          <th scope="col">Color</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($variants as $variant)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $variant->size }}</td>
            <td>{{ $variant->color }}</td>
            <td>
              <a href="{{ route('variant.edit', $variant->id) }}" class="badge bg-warning">
                <span data-feather="edit">Edit</span>
              </a>
              <form action="{{ route('variant.destroy', $variant->id) }}" method="POST" class="d-inline">
                @method('delete')
                @csrf
                <button class="badge bg-danger border-0"><span data-feather="x-circle">Delete</span></button>
              </form>
            </td>
        </tr>
        @endforeach
      </tbody>
    </table>
</div>
@endsection