@extends('main_menu.main')

@section('container')
<div class="container-fluid px-4">
   <h1 class="mt-4">Create Edit Variant</h1>
   <div class="card col-lg-8" style="padding: 13px; max-width: 30rem; box-shadow: 6px 8px rgb(0 0 0 / 4%); border: 1">
      <form action="{{ route('variant.update', $variant->id) }}" method="POST" class="mb-3" enctype="multipart/form-data">
      @csrf
      @method('PUT')
      <div class="mb-3">
         <label for="size" class="form-label">Size</label>
         <input type="text" class="form-control @error('size') is-invalid @enderror" id="size" name="size" placeholder="Size" required value="{{ $variant->size }}">
         @error('size')
            <div class="invalid-feedback">
               {{ $message }}
            </div>
         @enderror
      </div>
      <div class="mb-3">
         <label for="color" class="form-label">Color</label>
         <input type="text" class="form-control @error('color') is-invalid @enderror" id="color" name="color" placeholder="Color" required value="{{ $variant->color }}">
         @error('color')
            <div class="invalid-feedback">
               {{ $message }}
            </div>
         @enderror
      </div>
         <button type="submit" class="btn btn-primary">Create Edit Variant</button>
      </form>
   </div>   
</div>
@endsection