<?php

namespace App\Http\Controllers;

use App\Models\Variant;
use Illuminate\Http\Request;

class VariantController extends Controller
{
    public function index()
    {
        return view('variants.index', [
            'variants' => Variant::all()
        ]);
    }

    public function create()
    {
        return view('variants.create');
    }

    public function store(Request $request) 
    {
        $validate = $request->validate([
            'size' => 'required',
            'color' => 'required',
        ]);

        Variant::create($validate);
        return redirect()->route('variant.index')->with('success','New data created successfully');
    }

    public function edit(Variant $variant)
    {
        return view('variants.edit', [
            'variant' => $variant
        ]); 
    }

    public function update(Request $request, Variant $variant)
    {
        $rules = [
            'size' => 'required',
            'color' => 'required'
        ];

        $validatedData = $request->validate($rules);

        Variant::where('id', $variant->id)->update($validatedData);

        return redirect()->route('variant.index')->with('success','Updated data successfully');
    }

    public function destroy(Variant $variant)
    {
        $variant->delete();

        return redirect()->route('variant.index')->with('success','Deleted data successfully');
    }
}
