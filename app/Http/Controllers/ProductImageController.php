<?php

namespace App\Http\Controllers;

use App\Models\ProductImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('product_images.index', [
            'ProductImage' => ProductImage::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product_images.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name_image' => 'required',
            'image' => 'required|image|file|max:2024'
        ]);

        if($request->file('image') ) {
            $validatedData['image'] = $request->file('image')->store('product-images');
        }

        ProductImage::create($validatedData);
        return redirect()->route('product-image.index')->with('success','New data created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductImage  $productImage
     * @return \Illuminate\Http\Response
     */
    public function show(ProductImage $productImage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductImage  $productImage
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductImage $productImage)
    {
        return view('product_images.edit', [
            'productimage' => $productImage
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductImage  $productImage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductImage $productImage)
    {
        $rules = [
            'name_image' => 'required',
            'image' => 'required|image|file|max:2024'
        ];
        $validatedData = $request->validate($rules);
        if($request->file('image')){
            $validatedData['image'] = $request->file('image')->store('product-images');
            Storage::delete($productImage->image);
        }

        ProductImage::where('id', $productImage->id)->update($validatedData);
        return redirect()->route('product-image.index')->with('success','Updated data successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductImage  $productImage
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductImage $productImage)
    {
        Storage::delete($productImage->image);

        $productImage->delete();
        return redirect()->route('product-image.index')->with('success','Deleted data successfully');
    }
}
