<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Variant;
use App\Models\ProductCategory;
use App\Models\ProductImage;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = Product::all();
        return view('product.index', [
            'product' => $product
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product.create', [
            'variants' => Variant::all(),
            'productategoryList' => ProductCategory::all(),
            'productimageList' => ProductImage::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $request->validate([
            'name_product' => 'required',
            'description' => 'required',
            'price' => 'required',
            'name_variantId' => 'required',
            'product_categoryId' => 'required',
            'product_imageId' => 'required',
        ]);

        Product::create($validate);
        return redirect()->route('product.index')->with('success','New data created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $variant = Variant::where('id', '!=', $product->name_variantId)->get(['id', 'size','color']);
        $productategoryList = ProductCategory::where('id', '!=', $product->product_categoryId)->get(['id', 'name_category']);
        $productimageList = ProductImage::where('id', '!=', $product->product_imageId)->get(['id', 'name_image']);
        return view('product.edit', [
            'product' => $product,
            'variants' => $variant,
            'productategoryList' => $productategoryList,
            'productimageList' => $productimageList,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $rules = [
            'name_product' => 'required',
            'description' => 'required',
            'price' => 'required',
            'name_variantId' => 'required',
            'product_categoryId' => 'required',
            'product_imageId' => 'required',
        ];

        $validatedData = $request->validate($rules);

        Product::where('id', $product->id)->update($validatedData);
        return redirect()->route('product.index')->with('success','Updated data successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->route('product.index')->with('success','Deleted data successfully');
    }
}
