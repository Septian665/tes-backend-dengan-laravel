<?php

namespace App\Http\Controllers\Api;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProdukResource;
use App\Http\Resources\ProductDetailResource;

class ProductController extends Controller
{
    public function index()
    {
        $product = ProdukResource::collection(Product::all());

        return response()->json([
            'status' => true,
            'product' => $product
        ],200);
    }

    public function show($Product)
    {
        $result = Product::find($Product);
        if(!$result){
            return response()->json([
                'status' => false,
                'product' => 'product not found'
            ],404);
        }
        return response()->json([
            'status' => true,
            'product' => $result
        ],200);
    }
}
