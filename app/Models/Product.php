<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function variant()
    {
        return $this->belongsTo(Variant::class, 'name_variantId', 'id');
    }

    public function ProductImage()
    {
        return $this->belongsTo(ProductImage::class, 'product_imageId', 'id');
    }

    public function ProductCategory()
    {
        return $this->belongsTo(ProductCategory::class, 'product_categoryId', 'id');
    }
}
