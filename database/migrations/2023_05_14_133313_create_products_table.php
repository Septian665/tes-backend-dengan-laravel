<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name_product');
            $table->string('description');
            $table->integer('price');
            $table->unsignedBigInteger('name_variantId');
            $table->unsignedBigInteger('product_categoryId');
            $table->unsignedBigInteger('product_imageId');
            $table->foreign('name_variantId')->references('id')->on('variants');
            $table->foreign('product_categoryId')->references('id')->on('product_categories');
            $table->foreign('product_imageId')->references('id')->on('product_images');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
